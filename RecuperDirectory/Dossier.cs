﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace RecuperDirectory
{
    class Dossier
    {
        public Dossier(string chemin)
        {
            this.TDossier = Directory.GetDirectories(chemin);
            this.CheminD = chemin;
        }
        public string[] TDossier { get; set; }
        public string CheminD { get; set; }
        public void AfficherNbrDossier()
        {
            Console.WriteLine("Le nombre de Dossiers est {0}.", TDossier.Length);
        }
    }
}
