﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace RecuperDirectory
{
    class Fichier
    {
        public Fichier(string chemin)
        {
            this.TFichier = Directory.GetFiles(chemin);
            this.CheminF = chemin; 
        }
        public string[] TFichier { get; set; }
        public string CheminF { get; set; }
        public void AfficherNbrFichier()
        {
            Console.WriteLine("Le nombre de Dossiers est {0}.", TFichier.Length);
        }
        public void AfficherInfo()
        {
            string cheminFichier = new System.IO.FileInfo(CheminF).FullName;
            long tailleFichier = new System.IO.FileInfo(CheminF).Length;
            string nomFichier = new System.IO.FileInfo(CheminF).Name;
            string extensionFichier = new System.IO.FileInfo(CheminF).Extension;
            Console.WriteLine("Chemin du fichier {0} \nTaille du fichier {1} Ko\nNom du Fichier {2}\nExtension {3} \n",
                cheminFichier, tailleFichier, nomFichier, extensionFichier);
        }
    }
}
